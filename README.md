CartsGuru Library
=========================

The easiest way to connect your store to the Carts Guru API

Features
--------

* Cart and sales tracking
* Facebook Messenger
* Facebook Ads

Development
--------
![Pipeline status](https://img.shields.io/bitbucket/pipelines/market-wave/cartsguru-lib-php.svg)

Use docker
```
docker-compose up -d
```

Install composer
```
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
```

Install dependancies
```
composer install
```

Run tests
```
vendor/bin/phpunit 
```

Format files
```
php-cs-fixer fix .
```

Generate doc
vendor/bin/phpdoc run  --target doc -d src