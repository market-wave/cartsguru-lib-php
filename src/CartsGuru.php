<?php namespace CartsGuru\CartsGuru;

/**
 * Remote API connector
 *
 * @author Carts Guru
 * @copyright Copyright (c) Carts Guru
 * @version 1.0.0
 * @license LGPL-2.1
 *
 */

class CartsGuru
{
    /** @var string $params     Configuration  */
    protected $params;

    /** @var string $auth_key     Auth token sent to the api  */
    protected $auth_key;

    /** @var string $site_id     Site ID sent to the api  */
    protected $site_id;

    /** @var RemoteAPI $api     RemoteAPI instance  */
    protected $api;

    public function __construct($params = null)
    {
        $this->params = $params;
        if ($params) {
            $this->configure($params);
        }
    }

    // ----------------------------------------------------------- //
    // ------------  Virtual functions to override -------------- //
    // ----------------------------------------------------------- //

    /**
    * Adapt cart to Carts Guru requirement. Must be overriden
    *
    * @param object $cart Cart object to transform
    * @return object Ready to send object
    */
    public function adaptCart($cart)
    {
        throw new \Exception('adaptOrder must be overriden');
    }

    /**
    * Adapt order to Carts Guru requirement. Must be overriden
    *
    * @param object $order Order object to transform
    * @return object Ready to send object
    */
    public function adaptOrder($order)
    {
        throw new \Exception('adaptOrder must be overriden');
    }

    /**
    * Adapt product to Carts Guru requirement. Must be overriden
    *
    * @param object $product Product object to transform
    * @return object Ready to send object
    */
    public function adaptProduct($product)
    {
        throw new \Exception('adaptProduct must be overriden');
    }

    // ----------------------------------------------------------- //
    // ------------------  Public functions ---------------------- //
    // ----------------------------------------------------------- //

    /**
    * Generate the tag string regarding the context
    *
    * @param object $options      config, cart, order, product can be set regarding the context. Adapt function will be called
    * @return string Ready tag, must be put in front end
    */
    public function generateTag($options)
    {
        if (array_key_exists('template_folder', $this->params)) {
            $template = new Template('tag', $this->params['template_folder']);
        } else {
            $template = new Template('tag');
        }

        $params = array(
            'site_id' => $this->site_id
        );

        if (array_key_exists('config', $options)) {
            $params['config'] = $options['config'];
        } elseif (array_key_exists('config', $this->params)) {
            $params['config'] = $this->params['config'];
        } else {
            $params['config'] = null;
        }

        if (array_key_exists('cart', $options)) {
            $params['cart'] = $this->adaptCart($options['cart']);
        } else {
            $params['cart'] = null;
        }

        if (array_key_exists('order', $options)) {
            $params['order'] = $this->adaptOrder($options['order']);
        } else {
            $params['order'] = null;
        }

        if (array_key_exists('product', $options)) {
            $params['product'] = $this->adaptProduct($options['product']);
        } else {
            $params['product'] = null;
        }

        return $template->render($params);
    }

    /**
    * Adapt cart and send it to the API
    *
    * @param object $cart      Cart object
    * @return RemoteAPI RemoteAPI result
    */
    public function trackCart($cart)
    {
        return $this->doPost('/carts', $this->adaptCart($cart));
    }

    /**
    * Adapt order and send it to the API
    *
    * @param object $order      Order object
    * @return RemoteAPI RemoteAPI result
    */
    public function trackOrder($order)
    {
        return $this->doPost('/orders', $this->adaptOrder($order));
    }
 
    /**
    * Set params,  auth_key, site_id and api
    *
    * @param array $params      Options
    */
    public function configure($params)
    {
        $this->params = array_merge($this->params, $params);

        if (!array_key_exists('auth_key', $this->params)) {
            throw new \InvalidArgumentException('auth_key is required inside params');
        }

        $this->auth_key = $this->params['auth_key'];
        $this->site_id = array_key_exists('site_id', $this->params) ? $this->params['site_id'] : null;
        
        $api_options = array_key_exists('api_options', $this->params) ? $this->params['api_options'] : array();
        $this->api = new RemoteAPI($this->auth_key, $api_options);
    }

    // ----------------------------------------------------------- //
    // -----------------  Protected functions -------------------- //
    // ----------------------------------------------------------- //

    /**
    * Send data using remote API
    *
    * @param string $path           ex /orders,/carts
    * @param array $parameters      Query or body parameter
    * @param boolean $sync          Wait response
    * @return RemoteAPI RemoteAPI   with initialization
    */
    protected function doPost($path, $parameters, $sync = false)
    {
        if (!$this->api) {
            throw new \Exception('CartGuru is not configured');
        }
        
        return $this->api->post($path, $parameters, $sync);
    }

    /**
    * Get data using remote API
    *
    * @param string $path           ex /orders,/carts
    * @param array $parameters      Query or body parameter
    * @param boolean $sync          Wait response
    * @return RemoteAPI RemoteAPI   with initialization
    */
    protected function doGet($path, $parameters, $sync = false)
    {
        if (!$this->api) {
            throw new \Exception('CartGuru is not configured');
        }

        return $this->api->get($path, $parameters, $sync);
    }
}
