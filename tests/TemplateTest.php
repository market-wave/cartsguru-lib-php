<?php 
use PHPUnit\Framework\TestCase;

/**
*  Testing Template class
*
*  @author Carts Guru
*  @coversDefaultClass CartsGuru\CartsGuru\Template
*/
class TemplateTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     */
    public function isThereAnySyntaxError()
    {
        $template = new CartsGuru\CartsGuru\Template('filename');
        $this->assertTrue(is_object($template));
        unset($template);
    }

    /**
     * @test
     * @covers ::render
     */
    public function renderTagWithCart()
    {
        $template = new CartsGuru\CartsGuru\Template('tag');
    
        $params = array(
        'cart' => array(
            'id' => 23020,
            'email' => 'test@php.net'
        )
    );

        $content = $template->render($params);
    
        $this->assertContains('cart: JSON.parse(\'{"id":23020,"email":"test@php.net"}\')', $content);
    
        unset($template);
    }

    /**
     * @test
     * @covers ::render
     */
    public function renderTagWithOrder()
    {
        $template = new CartsGuru\CartsGuru\Template('tag');
    
        $params = array(
            'order' => array(
                'id' => 23020,
                'email' => 'test@php.net'
            )
        );

        $content = $template->render($params);
    
        $this->assertContains('order: JSON.parse(\'{"id":23020,"email":"test@php.net"}\')', $content);
    
        unset($template);
    }

    /**
     * @test
     * @covers ::render
     */
    public function renderTagWithProduct()
    {
        $template = new CartsGuru\CartsGuru\Template('tag');
    
        $params = array(
            'product' => array(
                'id' => 23020,
                'totalET' => 10.11
            )
        );

        $content = $template->render($params);
    
        $this->assertContains('product: JSON.parse(\'{"id":23020,"totalET":10.11}\')', $content);
    
        unset($template);
    }

    /**
     * @test
     * @covers ::render
     */
    public function renderTagWithNoProduct()
    {
        $template = new CartsGuru\CartsGuru\Template('tag');
    
        $params = array(
            'product' => null
        );

        $content = $template->render($params);
    
        $this->assertContains('product: JSON.parse(\'null\')', $content);
    
        unset($template);
    }
}
