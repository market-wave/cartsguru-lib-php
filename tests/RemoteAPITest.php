<?php 
use PHPUnit\Framework\TestCase;

/**
*  Testing RemoteAPI class
*
*  @author Carts Guru
*  @coversDefaultClass CartsGuru\CartsGuru\RemoteAPI
*/
class RemoteAPITest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     */
    public function isThereAnySyntaxError()
    {
        $controller = new CartsGuru\CartsGuru\RemoteAPI;
        $this->assertTrue(is_object($controller));
        unset($controller);
    }
  
    /**
     * @test
     * @covers ::set_option
     */
    public function isSetOptionDone()
    {
        $controller = new CartsGuru\CartsGuru\RemoteAPI;
    
        $controller->set_option('key', 'myvalue');
    
        $this->assertTrue($controller->options['key'] == 'myvalue');
    
        unset($controller);
    }
}
