<?php 
use PHPUnit\Framework\TestCase;
use CartsGuru\CartsGuru\CartsGuru;

/**
*  Testing CartsGuru class
*
*  @author Carts Guru
*  @coversDefaultClass CartsGuru\CartsGuru\CartsGuru
*/
class CartsGuruTest extends TestCase
{
    /**
     * @test
     * @covers ::__construct
     */
    public function isThereAnySyntaxError()
    {
        $controller = new CartsGuru();
        $this->assertTrue(is_object($controller));
        unset($controller);
    }

    /**
     * @test
     * @covers ::__construct
     */
    public function isAuthKeyRequired()
    {
        $this->expectException(InvalidArgumentException::class);

        $params = array(
        'fake' => 1
    );
        $controller = new CartsGuru($params);
    }

    /**
     * @test
     * @covers ::adaptCart
     */
    public function testAdaptCartMustBeOverriden()
    {
        $this->expectException(Exception::class);

        $cart = array();
        $controller = new CartsGuru();
        $controller->adaptCart($cart);
    }

    /**
     * @test
     * @covers ::adaptOrder
     */
    public function testAdaptOrderMustBeOverriden()
    {
        $this->expectException(Exception::class);

        $order = array();
        $controller = new CartsGuru();
        $controller->adaptOrder($order);
    }

    /**
     * @test
     * @covers ::adaptProduct
     */
    public function testAdaptProductMustBeOverriden()
    {
        $this->expectException(Exception::class);

        $product = array();
        $controller = new CartsGuru();
        $controller->adaptProduct($product);
    }

    /**
     * @test
     * @covers ::generateTag
     */
    public function testGenerateTagWithCart()
    {
        $controller = new CartsGuruController();
    
        $params = array(
            'cart' => array(
                'id' => 23020,
                'email' => 'test@php.net'
            )
         );

        $content = $controller->generateTag($params);
    
        $this->assertContains('cart: JSON.parse(\'{"id":23020,"email":"test@php.net"}\')', $content);
        $this->assertContains('order: JSON.parse(\'null\')', $content);
        $this->assertContains('product: JSON.parse(\'null\')', $content);
        $this->assertContains('config: JSON.parse(\'null\')', $content);
        $this->assertContains('https://t.carts.guru/bundle.js?sid=site-id-test', $content);
    }

    /**
     * @test
     * @covers ::generateTag
     */
    public function testGenerateTagWithOrder()
    {
        $controller = new CartsGuruController();
    
        $params = array(
            'order' => array(
                'id' => 23020,
                'email' => 'test@php.net'
            )
        );

        $content = $controller->generateTag($params);
    
        $this->assertContains('cart: JSON.parse(\'null\')', $content);
        $this->assertContains('order: JSON.parse(\'{"id":23020,"email":"test@php.net"}\')', $content);
        $this->assertContains('product: JSON.parse(\'null\')', $content);
        $this->assertContains('config: JSON.parse(\'null\')', $content);
        $this->assertContains('https://t.carts.guru/bundle.js?sid=site-id-test', $content);
    }

    /**
     * @test
     * @covers ::generateTag
     */
    public function testGenerateTagWithProduct()
    {
        $controller = new CartsGuruController();
    
        $params = array(
            'product' => array(
                'id' => 23020,
                'totalET' => 20.10,
                'label' => 'my product'
            )
        );

        $content = $controller->generateTag($params);
    
        $this->assertContains('cart: JSON.parse(\'null\')', $content);
        $this->assertContains('order: JSON.parse(\'null\')', $content);
        $this->assertContains('product: JSON.parse(\'{"id":23020,"totalET":20.1,"label":"my product"}\')', $content);
        $this->assertContains('config: JSON.parse(\'null\')', $content);
        $this->assertContains('https://t.carts.guru/bundle.js?sid=site-id-test', $content);
    }

    /**
     * @test
     * @covers ::generateTag
     */
    public function testGenerateTagWithConfig()
    {
        $controller = new CartsGuruController();
    
        $params = array(
            'config' => array(
                'feature' => array(
                    'enableTest' => true
                )
            )
        );

        $content = $controller->generateTag($params);
    
        $this->assertContains('cart: JSON.parse(\'null\')', $content);
        $this->assertContains('order: JSON.parse(\'null\')', $content);
        $this->assertContains('product: JSON.parse(\'null\')', $content);
        $this->assertContains('config: JSON.parse(\'{"feature":{"enableTest":true}}\')', $content);
        $this->assertContains('https://t.carts.guru/bundle.js?sid=site-id-test', $content);
    }

    /**
     * @test
     * @covers ::generateTag
     */
    public function testGenerateTagWithGlobalConfig()
    {
        $controller = new CartsGuruController();
    
        $params = array(
            'config' => array(
                'feature' => array(
                    'enableTest' => true
                )
            )
        );
        $controller->configure($params);

        $content = $controller->generateTag(array());
    
        $this->assertContains('cart: JSON.parse(\'null\')', $content);
        $this->assertContains('order: JSON.parse(\'null\')', $content);
        $this->assertContains('product: JSON.parse(\'null\')', $content);
        $this->assertContains('config: JSON.parse(\'{"feature":{"enableTest":true}}\')', $content);
        $this->assertContains('https://t.carts.guru/bundle.js?sid=site-id-test', $content);
    }
}

class CartsGuruController extends CartsGuru
{
    public function __construct()
    {
        $params = array(
            'site_id' => 'site-id-test',
            'auth_key' => 'auth-key-test',
        );
        parent::__construct($params);
    }

    public function adaptCart($item)
    {
        return $item;
    }

    public function adaptOrder($item)
    {
        return $item;
    }

    public function adaptProduct($item)
    {
        return $item;
    }
}
